// lib/api.js

var models = require('../models');

exports.tasks = {
  all: function(request, reply) {
    models.Task.findAll()
      .then(function(tasks) {
	  reply.view('index', {tasks});
      });
  },
  create: function(request, reply){
	models.Task.create({
		title: 'foo', 
		description: 'bar',
		duration: 7,
		points: 5,
		done: false,
	}).then(function(tasks) {
        reply(tasks).code(200);
      });
  } 
};
exports.groups = {
	create: function(request, reply){
		models.Group.create({
			title: 'putzen'
		}).then(function(groups) {
        reply(groups).code(200);
      });
	}
};