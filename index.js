'use strict';

var Path = require('path');
var Hapi = require('hapi');
var Hoek = require('hoek');
var Models = require('./models');
var Inert = require('inert');

var server = new Hapi.Server({
	connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'public')
            }
        }
    }
});
server.connection({ port: 8080 });
// routes
server.route(require('./lib/routes'));

server.route({
    method: 'GET',
    path: '/',
    handler: function (request, reply) {
        reply.view('index');
    }
});

server.route({
    method: 'GET',
    path: '/statistics',
    handler: function (request, reply) {
        reply.view('statistics');
    }
});

server.route({
    method: 'GET',
    path: '/players',
    handler: function (request, reply) {
        reply.view('players');
    }
});

server.register(Inert, () => {});

server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
        directory: {
            path: '.',
            redirectToSlash: true,
            index: true
        }
    }
});

server.register(require('vision'), (err) => {

Hoek.assert(!err, err);

	    server.views({
			engines: {
				html: require('handlebars')
			},
			relativeTo: __dirname,
			path: './templates',
			layoutPath: './templates/layout',
			helpersPath: './templates/helpers'
		});
});



Models.sequelize.sync().then(function() {
  server.start(function() {
    console.log('Running on 8080');
  });
});

server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});