'use strict';
module.exports = function(sequelize, DataTypes) {
  var Handler = sequelize.define('Handler', {
    title: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Handler;
};