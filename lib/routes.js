// lib/routes.js

var api = require('./api');

module.exports = [
  {
    method: 'GET',
    path: '/tasklist',
    handler: api.tasks.all
  }, 
  {
	method: 'POST', 
	path: '/tasklist/add',
	handler: api.tasks.create
  },
  {
	method: 'POST', 
	path: '/group/add',
	handler: api.groups.create
  }
];