'use strict';
module.exports = function(sequelize, DataTypes) {
  var Task = sequelize.define('Task', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    duration: DataTypes.INTEGER,
    points: DataTypes.INTEGER,
    done: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        Task.hasOne(models.Handler);
		Task.hasOne(models.Group);
      }
    }
  });
  return Task;
};